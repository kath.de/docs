## Softwareplattform

* Ubuntu (16.04.1)

### Webstack
* Nginx (1.10.0)
* php7.0-fpm (7.0.8)

### Datenbanken
* MySQL (5.7)
* Redis (3.0.6)

### Tools
* OpenSSL (1.0.2g)
* GraphicsMagic (1.3.23)

## SSH-Zugang
Das Hauptverzeichnis der Webanwendungen befindet sich in `/data/web/*`

Im Home-Verzeichnis des Nutzeraccounts befinden sich in der Datei `.my.cnf` die Zugangsdaten zur MySQL-Datenbank für die Nutzung durch den Kommandozeilen-Client. Innerhalb der Ausführungsumgebung der Anwendung sind diese auch als Umgebungsvariablen definiert: `DB_NAME`, `DB_USER`, `DB_HOST`, 
`DB_PASSWORD`.

Die Datenbank ist nur über den localhost erreichbar, ein Zugriff von außen ist über einen SSH-Tunnel möglich.

## Verzeichnisstruktur:
* `nginx.conf` - Link zur Webserver-Konfigurationsdatei für die Site.
* `nginx.conf.d` - Link zum Verzeichnis mit weiteren Konfigurationsdateien.
* `logs` - Enhält Links auf die Access- und Errorlogs.
* `releases` - Enthält einzelne Releases der Anwendung, die automatisch bei einem `git-push` auf das Deployment-Repository angelegt werden. Ältere releases werden automatisch gelöscht.
* `current` - Link auf das aktive Release.
    - `public` - Document root für den Webserver
    - `bin/setup` - optionales Initialisierungscript
    - `bin/activate` - optionales Aktivierungsscript
    - `bin/deactivate` - optionales Deaktivierungsscript
* `shared` - Dieses Verzeichnis enthält beständige Daten wie User-Uploads, die unabhängig von einzelnen Releases sind und nicht im SCM erfasst werden. Über symbolische Links kann eine Einbindung in den Document Root erfolgen. Nur dieses Verzeichnis wird (neben der Datenbank) vom Backup erfasst.

## Deployment
Das Deployment erfolgt per Git push in das entsprechende Repository. Daraufhin wird der aktuelle Stand des `master`-Branch automatisch in das `releases`-Verzeichnis ausgecheckt und aktiviert.

Manuell kann ein Release mit dem Script `deploy-build <app> [<repo>]` ausgecheckt und per `deploy-activate <app> [<release>]` aktiviert werden.

## Umgebungsvariablen
Jede Instanz eines Applikationsservers ist mit Umgebungsvariablen ausgestattet, die Zugangsdaten für andere Dienste sowie sonstige sicherheitsrelevanten oder umgebungsabhängige Konfigurationsdaten enthalten.
Üblicherweise sind die folgenden Variablen gesetzt:
* `DB_TYPE`: `mysql` oder `postgres`
* `DB_NAME`
* `DB_USER`
* `DB_HOST`
* `DB_PASSWORD`
* `REDIS_HOST`
* `REDIS_PORT`
